//
//  main.m
//  gMap using json
//
//  Created by clicklabs124 on 11/3/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
